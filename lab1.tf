terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "2.60.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "lab01-rg" {
  name     = "lab01-rg"
  location = "Canada Central"
}

resource "azurerm_virtual_network" "lab1-vnet" {
  name                = "virtualNetwork1"
  location            = azurerm_resource_group.lab01-rg.location
  resource_group_name = azurerm_resource_group.lab01-rg.name
  address_space       = ["10.0.0.0/16"]
  dns_servers         = ["10.0.0.4", "10.0.0.5"]
}

resource "azurerm_subnet" "lab01-subnet1" {
  name                 = "lab01-subnet1"
  resource_group_name  = azurerm_resource_group.lab01-rg.name
  virtual_network_name = azurerm_virtual_network.lab1-vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_subnet" "lab01-subnet2" {
  name = "labe01-subnet2"
  resource_group_name = azurerm_resource_group.lab01-rg.name
  virtual_network_name = azurerm_virtual_network.lab1-vnet.name
  address_prefixes = [ "10.0.2.0/24" ]
}

resource "azurerm_network_security_group" "lab01-nsg" {
  name                = "lab01-nsg"
  resource_group_name = azurerm_resource_group.lab01-rg.name
  location            = azurerm_resource_group.lab01-rg.location

  security_rule {
    name                       = "rule1"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "lab01-nsg2" {
  name                = "lab01-nsg2"
  resource_group_name = azurerm_resource_group.lab01-rg.name
  location            = azurerm_resource_group.lab01-rg.location

  security_rule {
    name                       = "rule1"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "lab01-snsga" {
  subnet_id                 = azurerm_subnet.lab01-subnet1.id
  network_security_group_id = azurerm_network_security_group.lab01-nsg.id
}

resource "azurerm_subnet_network_security_group_association" "lab01-snsga2" {
  subnet_id = azurerm_subnet.lab01-subnet2.id
  network_security_group_id = azurerm_network_security_group.lab01-nsg2.id
}
